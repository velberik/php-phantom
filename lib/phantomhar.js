function PhantomHAR() {
    this.system = require('system');
    this.dataFolder = 'benign_samles';
    this.domain = '';
    this.visiturl = '';
    this.redirectsMain = [];
    this.redirectsFrame = [];
    this.frameLevel = 'root';
    this.frames = [];
    this.finalUrl = '';
    this.har = {};

    this.init = function () {
        if (this.system.args.length === 1) {
            console.log('Usage: loadspeed.js <some URL>');
            phantom.exit();
        }
        this.domain = this.system.args[1];
        this.visiturl = this.domain.replace(/\//g,"_");
    };

    this.createHAR = function(page) {
        var entries = [];

        page.resources.forEach(function (resource) {
            var request = resource.request,
                startReply = resource.startReply,
                endReply = resource.endReply;

            if (!request || !startReply || !endReply) {
                return;
            }

            // Exclude data URIs from the HAR because they aren't
            // included in the spec.
            if (request.url.substring(0, 5).toLowerCase() === 'data:') {
                return;
            }

            entries.push({
                startedDateTime: request.time.toISOString(),
                time: endReply.time - request.time,
                request: {
                    method: request.method,
                    url: request.url,
                    httpVersion: "HTTP/1.1",
                    cookies: [],
                    headers: request.headers,
                    queryString: [],
                    headersSize: -1,
                    bodySize: -1
                },
                response: {
                    status: endReply.status,
                    statusText: endReply.statusText,
                    httpVersion: "HTTP/1.1",
                    cookies: [],
                    headers: endReply.headers,
                    redirectURL: "",
                    headersSize: -1,
                    bodySize: startReply.bodySize,
                    content: {
                        size: startReply.bodySize,
                        mimeType: endReply.contentType
                    }
                },
                cache: {},
                timings: {
                    blocked: 0,
                    dns: -1,
                    connect: -1,
                    send: 0,
                    wait: startReply.time - request.time,
                    receive: endReply.time - startReply.time,
                    ssl: -1
                },
                pageref: page.address
            });
        });

        this.har = {
            log: {
                version: '1.2',
                creator: {
                    name: "PhantomJS",
                    version: phantom.version.major + '.' + phantom.version.minor +
                    '.' + phantom.version.patch
                },
                pages: [{
                    startedDateTime: page.startTime.toISOString(),
                    id: page.address,
                    title: page.title,
                    pageTimings: {
                        onLoad: page.endTime - page.startTime
                    }
                }],
                entries: entries
            }
        };
    };

    this.capture = function (page) {
        page.render(this.dataFolder + '/' + this.visiturl + '/SCRNSHT_' + this.visiturl + '.png');
    };

    this.getFinalUrl = function (page) {
        this.finalUrl = page.evaluate(function () {
            return window.location.href;
        });
    };

    // This function recursively finds all (nested) frames on the web page and saves them to seperate HTML files.
    this.recursiveFrameScan = function (page) {
        this.frames.push({
            content: page.frameContent,
            frameLevel: this.frameLevel
        });
        for(var i = 0; i < page.framesCount; i++) {
            page.switchToFrame(i);
            this.frameLevel += "-" + i.toString();
            this.recursiveFrameScan(page);
            page.switchToParentFrame();
            this.frameLevel = this.frameLevel.substring(0, this.frameLevel.length - (i.toString().length + 1))
        }
    };

    this.echoPage = function () {
        var echo = {
            har: this.har,
            redirectsMain: this.redirectsMain,
            redirectsFrame: this.redirectsFrame,
            finalUrl: this.finalUrl,
            frames: this.frames.map(function (value) {
                return {
                    frameLevel: value.frameLevel
                }
            })
        };

        // echo all page info in json to string
        console.log('<<<STARTPAGE' + JSON.stringify(echo, undefined, 4) + '<<<ENDPAGE');

        // echo html content of all frames separate, avoiding json converts
        console.log('<<<STARTFRAMES');
        this.frames.forEach(function (value) {
            var wraper = '<<<FRAME-' + value.frameLevel.toUpperCase();
            console.log(wraper + value.content + wraper);
        });
        console.log('<<<ENDFRAMES');
    };
}

module.exports = new PhantomHAR();