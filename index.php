<?php

require 'Classes/Phantom.php';

$phantom = new Phantom();

$domains = ['www.bing.com', 'www.w3schools.com', 'google.com.ua', 'amazon.com', 'www.yahoo.com'];

foreach ($domains as $domain) {
    $page = $phantom->run($domain);
    var_dump($page->frames[0]['content']);
    break;
}

// @TODO:
// 1. Add Exceptions
// 2. Add comments
// 3. Move to "crawler"
// 4. Crete tests
