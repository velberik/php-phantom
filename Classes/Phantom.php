<?php

require 'Page.php';

class Phantom
{
    const PHANTOM_JS = 'phantomjs';
    const JS_FILE = 'php-phantom.js';

    public $page;
    private $options = array();
    private $phantomjs;

    public function __construct($path2phantom = '')
    {
        $this->phantomjs = $path2phantom . static::PHANTOM_JS;
    }

    /**
     * run the PhantomJS script and return the stdOut
     * in using the output variable
     *
     * @param $domain
     * @return array
     * @throws Exception
     */
    public function run($domain)
    {
        // options parsing
        $options = '';
        foreach ($this->options as $option => $value) {
            $options .= ' --' . $option . '=' . $value;
        }

        $command = sprintf("%s %s %s", $this->phantomjs, static::JS_FILE, $domain);
        exec($command, $output);

        if (empty($output)) {
            throw new \Exception('Can not find PhantomJS.');
        }

        $output = implode('', $output);
        if (false !== strpos($output, 'FAIL to load the address')) {
            throw new \Exception('FAIL to load the address.');
        }

        return $this->parseOutput($output);
    }

    private function parseOutput($output)
    {
        preg_match('/<<<STARTPAGE(.*)<<<ENDPAGE/', $output, $matches);
        if (isset($matches[1])) {
            $data = json_decode($matches[1], true);
            $page = new Page($data);
        } else {
            throw new \Exception(substr($output, 0, 100));
        }

        preg_match('/<<<STARTFRAMES(.*)<<<ENDFRAMES/', $output, $matches);

        if (isset($matches[1])) {
            array_walk($page->frames, function (&$frame) use ($matches) {
                $pattern = '<<<FRAME-' . strtoupper($frame['frameLevel']);
                preg_match("/$pattern(.*)$pattern/", $matches[1], $content);
                if (isset($content[1])) {
                    $frame['content'] = $content[1];
                }
            });
        } else {
            throw new \Exception(substr($output, 0, 100));
        }

        return $page;
    }
}
