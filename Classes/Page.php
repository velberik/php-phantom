<?php

class Page
{
    public $har;

    public $redirectsMain;

    public $redirectsFrame;

    public $finalUrl;

    public $frames;

    public function __construct(array $data)
    {
        $this->har = $data['har'];
        $this->redirectsMain = $data['redirectsMain'];
        $this->redirectsFrame = $data['redirectsFrame'];
        $this->finalUrl = $data['finalUrl'];
        $this->frames = $data['frames'];
    }

    public function isMainFrame($frameLevel)
    {
        return 'root' === $frameLevel;
    }
}
