if (!Date.prototype.toISOString) {
    Date.prototype.toISOString = function () {
        function pad(n) { return n < 10 ? '0' + n : n; }
        function ms(n) { return n < 10 ? '00'+ n : n < 100 ? '0' + n : n }
        return this.getFullYear() + '-' +
            pad(this.getMonth() + 1) + '-' +
            pad(this.getDate()) + 'T' +
            pad(this.getHours()) + ':' +
            pad(this.getMinutes()) + ':' +
            pad(this.getSeconds()) + '.' +
            ms(this.getMilliseconds()) + 'Z';
    }
}

var phantomHar = require('./lib/phantomhar');
phantomHar.init();

var page = require('webpage').create();
page.address = 'http://' + phantomHar.domain;
page.resources = [];
page.viewportSize = {
    width: 1024,
    height: 768
};

page.onLoadStarted = function () {
    page.startTime = new Date();
};

page.onResourceRequested = function (req) {
    page.resources[req.id] = {
        request: req,
        startReply: null,
        endReply: null
    };
};

page.onResourceReceived = function (res) {
    if (res.stage === 'start') {
        page.resources[res.id].startReply = res;
    }
    if (res.stage === 'end') {
        page.resources[res.id].endReply = res;
    }
};

// Record all redirection hops
page.onNavigationRequested = function(url, type, willNavigate, main) {

    console.log('> Trying to navigate to: ' + url + ' (main frame: ' + main + ')');

    // Check if the navigation resulted from a redirection
    if (type=="Other" || type=="Undefined") {

        // Check if it affects the main frame or another one
        if (main) {

            // Main frame is redirecting.
            phantomHar.redirectsMain.push(url);
        } else {

            // Other frame is redirecting. Logging to file.
            phantomHar.redirectsFrame.push(url);
        }
    }
};

page.open(page.address, function (status) {

    var har;

    if (status !== 'success') {
        console.log('FAIL to load the address');
        console.log(status);
    } else {
        try {
            page.endTime = new Date();
            page.title = page.evaluate(function () {
                return document.title;
            });

            // Create the HAR file using netsniff.js code (below)
            console.log('Creating HAR file.');
            phantomHar.createHAR(page);

            // Save all frames using the recursive frame scan
            console.log('Get all frames.');
            phantomHar.recursiveFrameScan(page);

            // Log the final url of the web page
            console.log('Get the final URL of the page.');
            phantomHar.getFinalUrl(page);

            phantomHar.echoPage();
        } catch (error) {
            console.log(error);
        }
    }

    phantom.exit();
});